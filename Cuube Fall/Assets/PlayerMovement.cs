﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float velocidad;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxisRaw ("Horizontal") < 0) {
			transform.Translate (velocidad * Time.deltaTime * Vector2.left);
		}
		if (Input.GetAxisRaw ("Horizontal") > 0) {
			transform.Translate (velocidad * Time.deltaTime * Vector2.right);
		}
	}
}
