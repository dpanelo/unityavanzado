﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EveryPlayIntegration : MonoBehaviour {

	public void OnReadyForRecording(bool enabled){
		if(enabled){
			Debug.LogAssertion(Everyplay.IsSupported());
			Debug.LogAssertion(Everyplay.IsRecordingSupported());
		}
		else{
			Debug.Log("Recording not ready.");
		}
	}

	// Use this for initialization
	void Start () {
		Everyplay.ReadyForRecording += OnReadyForRecording;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
