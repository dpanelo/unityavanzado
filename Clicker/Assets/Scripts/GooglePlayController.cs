﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GooglePlayEvents{
	public const string FirstTap =  "CgkIhInYuIQUEAIQAQ";
	public const string HundredTaps =  "CgkIhInYuIQUEAIQAg";
	public const string ThousandTaps =  "CgkIhInYuIQUEAIQAw";
	public const string GoHome =  " CgkIhInYuIQUEAIQBA";
}

public class GooglePlayLeaderboards{
	public const string Scoreboard = "CgkIhInYuIQUEAIQBQ";
}


public class GooglePlayController : MonoBehaviour {
	public Button SignInButton;
	private Score ScoreManager;

	void Awake(){
		ScoreManager = GetComponentInChildren<Score>();
	}

	// Use this for initialization
	void Start () {
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();
	}
	
	public void SignInClicked(){
		Social.localUser.Authenticate(OnSignIn);
	}

	public void SendToLeaderboardClicked(){
		int score = ScoreManager.GetScore();

		if(score > 0){
			Social.ReportScore(score, GooglePlayLeaderboards.Scoreboard, OnLeaderboardReport);
		}
	}

	private void OnSignIn(bool success){
		StatusTextController.StatusUpdated.Invoke("Signing in. Please Wait.");

		if(success){
			SignInButton.gameObject.SetActive(false);
			StatusTextController.StatusUpdated.Invoke("Signed in!");
		}
		else{
			StatusTextController.StatusUpdated.Invoke("Failed to sign in.");
		}
	}

	private void OnLeaderboardReport(bool success){
		if(success){
			StatusTextController.StatusUpdated.Invoke("Score uploaded.");
		}
		else{
			StatusTextController.StatusUpdated.Invoke("Leaderboard update failed.");
		}
	}

	public void ViewAchievements(){
		Social.ShowAchievementsUI();
	}

	public void ViewLeaderboard(){
		Social.ShowLeaderboardUI();
	}
}
