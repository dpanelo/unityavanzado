﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ChatManager : NetworkManager {

	public override void OnServerDisconnect(NetworkConnection conn){
		Debug.Log("Server OnServerDisconnect");
		Debug.LogAssertion(conn != null);
		Debug.LogAssertion(!string.IsNullOrEmpty(conn.address));
		ChatClient.ServerDisconnectedEvent.Invoke(conn);
		base.OnServerDisconnect(conn);
	}

	public override void OnClientDisconnect(NetworkConnection conn){
		Debug.Log("Server OnCLientDisconnect");
		Debug.LogAssertion(conn != null && !string.IsNullOrEmpty(conn.address));
		ChatClient.ClientDisconnectedEvent.Invoke(conn);
		base.OnClientDisconnect(conn);
	}

	public override void OnClientError(NetworkConnection conn, int errorCode){
		ChatClient.ClientNetworkError.Invoke(conn, errorCode);
		base.OnClientError(conn, errorCode);
	}

	public override void OnServerError(NetworkConnection conn, int errorCode){
		ChatClient.ServerNetworkError.Invoke(conn, errorCode);
		base.OnServerError(conn, errorCode);
	}

	public override void OnStopServer(){
		ChatClient.ServerStopped.Invoke();
		base.OnStopServer();
	}
}