﻿using UnityEngine;
using UnityEditor;
using System.Text;

public class Screenshots {

	[MenuItem("Custom Tools/Take Screenshot")]
	public static void TakeScreenshot(){

		var folderName = Application.dataPath + "/Screenshots";
		Debug.Log (folderName);
		if(!System.IO.File.Exists(folderName)){
			Debug.Log("folder: \"" + folderName + "\" doesn't exist. Creating");
			AssetDatabase.CreateFolder("Assets", "Screenshots");
		}

		var sb = new StringBuilder (folderName + "/screenshot0.png");
		var i = 0;

		while(System.IO.File.Exists(sb.ToString())){
			Debug.Log ("\"screenshot"+ i + ".png\" already exists.");
			i++;
			sb.Replace(sb.ToString(), (folderName + "/screenshot" + i + ".png"));
		}

		ScreenCapture.CaptureScreenshot (sb.ToString());
		Debug.Log ("screenshot" + i + ".png taken");
	}
}
