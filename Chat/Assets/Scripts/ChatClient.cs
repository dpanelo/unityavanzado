﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class  ServerDisconnectedEvent: UnityEvent<NetworkConnection>{}
[System.Serializable]
public class  ClientDisconnectedEvent: UnityEvent<NetworkConnection>{}
[System.Serializable]
public class  ServerNetworkErrorEvent: UnityEvent<NetworkConnection, int>{}
[System.Serializable]
public class  ClientNetworkErrorEvent: UnityEvent<NetworkConnection, int>{}

public class ChatClient : NetworkBehaviour {
	private InputField ChatInput, PlayerName;
	private Text Console;
    private Button SendButton;

	public static ClientDisconnectedEvent ClientDisconnectedEvent;
    public static ServerDisconnectedEvent ServerDisconnectedEvent;
    public static ClientNetworkErrorEvent ClientNetworkError;
    public static ServerNetworkErrorEvent ServerNetworkError;
    public static UnityEvent ServerStopped;

    private string currentIp;
    private Dictionary<NetworkInstanceId, string> nameList;

    void Awake(){
        nameList = new Dictionary<NetworkInstanceId, string>();
        
        if(ServerDisconnectedEvent == null){
            ServerDisconnectedEvent = new ServerDisconnectedEvent();
        }
        if(ServerNetworkError == null){
            ServerNetworkError = new ServerNetworkErrorEvent();
        }
        if(ClientDisconnectedEvent == null){
            ClientDisconnectedEvent = new ClientDisconnectedEvent();
        }
        if(ClientNetworkError == null){
            ClientNetworkError = new ClientNetworkErrorEvent();
        }
        if(ServerStopped == null){
            ServerStopped = new UnityEvent();
        }

        Console = GameObject.FindGameObjectWithTag("Console").GetComponent<Text>();
        ChatInput = GameObject.FindGameObjectWithTag("ChatInput").GetComponent<InputField>();
        PlayerName = GameObject.FindGameObjectWithTag("PlayerName").GetComponent<InputField>();
        SendButton = GameObject.FindGameObjectWithTag("SendButton").GetComponent<Button>();

//Refactorear esto para usar NetworkInstanceId
/*
        ServerDisconnectedEvent.AddListener(PlayerDisconnected);
        ClientDisconnectedEvent.AddListener(DisconnectedFromServer);
        ServerNetworkError.AddListener(ServerNetworkErrorListener);
        ClientNetworkError.AddListener(ClientNetworkErrorListener);
        ServerStopped.AddListener(ServerStoppedListener);
        */
    }

	public override void OnStartServer(){
        if(isServer){
            Debug.Log ("Server Initialized");
            Console.text += "\n" + "Server started.";
        }
	}

    public override void OnStartLocalPlayer(){
        Debug.Log(isClient);
        Debug.Log(isLocalPlayer);
        Debug.Log("ConnectionToServer: " + connectionToServer != null);
        Debug.Log("ConnectionToClient: " + connectionToClient != null);
        Debug.Log("Server Ip: " + connectionToServer.address);
        
        if(isLocalPlayer){
            

            SendButton.onClick.AddListener(SendChat);

            if(!string.IsNullOrEmpty(PlayerName.text)){
                //A lo bruto, TODO: refactorear bien.
                CmdSetPlayerName(gameObject.GetComponent<NetworkIdentity>().netId, PlayerName.text);
            }

            if(!isServer){
                Console.text += "\nConnected to server.";
            }
        }
    }

    [Server]
    public void PlayerDisconnected(NetworkConnection conn){
        Debug.Log("PlayerDisconnectedListener");
        Debug.Log("conn.address : " + conn.address);
        RpcSendMessage(nameList[gameObject.GetComponent<NetworkIdentity>().netId] + " has disconnected.");
        nameList.Remove(gameObject.GetComponent<NetworkIdentity>().netId);
    }

    [Server]
    public void ServerNetworkErrorListener(NetworkConnection conn, int errorCode){
        RpcSendMessage(nameList[gameObject.GetComponent<NetworkIdentity>().netId] + " has lost connection.");
        nameList.Remove(gameObject.GetComponent<NetworkIdentity>().netId);
    }

    [Server]
    public void ServerStoppedListener(){
        Console.text += "\nServer stopped.";
    }

    [Client]
    public void DisconnectedFromServer(NetworkConnection conn){
        if(isLocalPlayer){
                Console.text += "\nDisconnected from server.";
        }
    }

    [Client]
    public void ClientNetworkErrorListener(NetworkConnection conn,  int errorCode){
        if(isLocalPlayer){
            Console.text += "\nLost connection to server.";
        }
    }

    public void SendChat(){
        if(isLocalPlayer){
            Debug.Log("IsLocalPlayer=" + isLocalPlayer);
            CmdReceiveChat(ChatInput.text, gameObject.GetComponent<NetworkIdentity>().netId);
        }
    }

    [Command]
    private void CmdReceiveChat(string message, NetworkInstanceId id){
        string finalMessage = null;
        if(nameList.ContainsKey(id)){
            finalMessage = nameList[id] + ": " + message;
        }
        else{
            finalMessage = id + ": " + message;
        }

        RpcSendMessage(finalMessage);
    }

    [Command]
    private void CmdSetPlayerName(NetworkInstanceId networkId, string playerName){
        if(!nameList.ContainsKey(networkId)){
            nameList.Add(networkId, playerName);   
        }
        else{
            nameList[networkId] = playerName;
        }
    }

    [ClientRpc]
    private void RpcSendMessage(string message){
        Console.text += "\n" + message;
    }
}