﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class DarPuntosYDesaparecer : MonoBehaviour {
	public float puntosADar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D col){
		var otherColliderObject = col.gameObject;

		if (otherColliderObject.CompareTag ("Player")) {
			otherColliderObject.GetComponent<PlayerPoints> ().DarPuntos (puntosADar);
			Destruir ();
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Bottom")) {
			Destruir ();
		}
	}

	void Destruir(){
		Destroy (gameObject);
	}
}
