﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.Analytics;
using UnityEngine.Advertisements;

public class Tapper : MonoBehaviour {
	private int timesTapped;
	public int leftClickBonus;

	void Start(){
		timesTapped = 0;
	}
	
	// Update is called once per frame
	public void ButtonClicked(){
		Score.ScoreIncreased.Invoke(leftClickBonus);

		timesTapped++;
		CheckTaps();
	}

//Refactorear para mover lógica de reportProgress a GooglePlayController
	private void CheckTaps(){
		if(timesTapped <= 1500 && timesTapped > 0){
			switch(timesTapped){
				case 1:
					Social.ReportProgress(GooglePlayEvents.FirstTap, 100f, ReportProgressSuccess);
					Analytics.CustomEvent("FirstTimeClicked");
					break;
				case 100:
					Social.ReportProgress(GooglePlayEvents.HundredTaps, 100f, ReportProgressSuccess);
					ShowRewardedAd();
					break;
				case 1000:
					Social.ReportProgress(GooglePlayEvents.ThousandTaps, 100f, ReportProgressSuccess);
					break;
				case 1500:
					Social.ReportProgress(GooglePlayEvents.GoHome, 100f, ReportProgressSuccess);
					break;
			}
		}
	}

	private void ReportProgressSuccess(bool success){
		if(!success){
			StatusTextController.StatusUpdated.Invoke("Achievement unlock failed.");
		}
	}

	private void ShowRewardedAd(){
		if (Advertisement.IsReady("rewardedVideo"))
		{
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show("rewardedVideo", options);
		}
	}

	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
			case ShowResult.Finished:
			Debug.Log("The ad was successfully shown.");
			//
			// YOUR CODE TO REWARD THE GAMER
			// Give coins etc.
			break;
			case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
			case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
		}
	}
}
