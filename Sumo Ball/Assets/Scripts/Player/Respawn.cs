﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
public class Respawn : NetworkBehaviour {
	private Transform spawnPoint;
	// Use this for initialization
	private Rigidbody rigidbody;
	private MeshRenderer renderer;

	public static int MaxPlayerLives;
	[SyncVar]
	private int currentPlayerLives;
	[SyncVar]
	private int playerNumber;

	void Awake(){
		rigidbody = GetComponent<Rigidbody>();
		renderer = GetComponent<MeshRenderer>();
		spawnPoint = GameObject.FindGameObjectsWithTag("SpawnPoint")[0].transform;
	}
	void Start(){
		currentPlayerLives = MaxPlayerLives;
		
	}
	void OnTriggerExit(Collider other){
		renderer.enabled = false;
		rigidbody.Sleep();
		rigidbody.MovePosition(spawnPoint.position);
		renderer.enabled = true;
		rigidbody.WakeUp();
	}
}
