﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour {
	public Text Title;
	public Wave[] Waves;
	public GameObject[] Enemies;
	public int initialPoolAmount;
	private List<GameObject> RocksPool, ShipsPool;
	public bool LetPoolsGrow;

	private int currentWaveIndex, totalEnemies;
	private Wave currentWave;
	private StringBuilder WaveTitle;
	// Use this for initialization
	void Start () {
		WaveTitle = new StringBuilder("Wave");
		RocksPool = new List<GameObject>();
		ShipsPool = new List<GameObject>();

		currentWaveIndex = totalEnemies = 0;
		Enemies[0] = (GameObject)Resources.Load("Prefabs/Rock");
		Debug.Log("Loaded Rock");
		Enemies[1] = (GameObject)Resources.Load("Prefabs/EnemyShip");
		Debug.Log("Loaded Ship");

		PoolRocks();
		PoolShips();

		Initialize();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Initialize(){
		ReadWaveData();
		ReplaceWaveTitle("Wave " + currentWave.WaveNumber);
		StartWave();
	}

	void ReadWaveData(){
		currentWave = Waves[currentWaveIndex];
		totalEnemies = 0;
		totalEnemies += currentWave.RocksNumber;
		totalEnemies += currentWave.ShipsNumber;
	}

	void ReplaceWaveTitle(string title){
		WaveTitle.Replace(WaveTitle.ToString(), title);
		Title.text = WaveTitle.ToString();
	}

	void StartWave(){
		MakeRocks();
		MakeShips();
	}

	void PoolRocks(){
		for(int i = 0; i < initialPoolAmount; i++){
			GameObject rock = (GameObject)Instantiate(Enemies[0]);
			rock.GetComponent<EnemyCollider>().SetWaveManager(this);
			rock.SetActive(false);
			RocksPool.Add(rock);
		}
	}

	void PoolShips(){
		for(int i = 0; i < initialPoolAmount; i++){
			GameObject ship = (GameObject)Instantiate(Enemies[1]);
			ship.GetComponent<EnemyCollider>().SetWaveManager(this);
			ship.SetActive(false);
			RocksPool.Add(ship);
		}
	}

	GameObject GetFromPool(List<GameObject> pool){
		for(int i = 0; i < pool.Count; i++){
			if(!pool[i].activeInHierarchy){
				return pool[i];
			}
		}

		return null;	
	}

	GameObject GetRock(){
		var rock = GetFromPool(RocksPool);
		if(rock == null){
			if(LetPoolsGrow){
				rock = (GameObject)Instantiate(Enemies[0]);
				rock.GetComponent<EnemyCollider>().SetWaveManager(this);
				RocksPool.Add(rock);
			}
		}

		return rock;
	}

	GameObject GetShip(){
		var ship = GetFromPool(ShipsPool);

		if(ship == null){
			if(LetPoolsGrow){
				ship = (GameObject)Instantiate(Enemies[1]);
				ship.GetComponent<EnemyCollider>().SetWaveManager(this);
				ShipsPool.Add(ship);
			}
		}

		return ship;
	}

	void MakeRocks(){
		if(currentWave.RocksNumber > 0){
			var position = Vector3.zero;
			for(int i = 0; i < currentWave.RocksNumber; i++)
			{
				position.x = Random.Range(-5, 5);
				position.y = Random.Range(6, 8);
				
				var rock = GetRock();
				if(rock == null){
					return;
				}

				rock.transform.position = position;
				rock.transform.rotation = Quaternion.identity;

				if(!rock.activeInHierarchy){
					rock.SetActive(true);
				}
			}
		}
	}

	void MakeShips(){
		if(currentWave.ShipsNumber > 0){
			var position = Vector3.zero;
			for(int i = 0; i < currentWave.ShipsNumber; i++)
			{
				position.x = Random.Range(-5, 5);
				position.y = Random.Range(6, 8);

				var ship = GetShip();
				
				if(ship == null){
					return;
				}

				ship.transform.position = position;
				ship.transform.rotation = Quaternion.identity;

				if(!ship.activeInHierarchy){
					ship.SetActive(true);
				}
			}
		}
	}

	public void EnemyDestroyed(){
		totalEnemies--;
		if(totalEnemies <= 0){
			currentWaveIndex++;
			if(currentWaveIndex >= Waves.Length){
				ReplaceWaveTitle("Game Over");
			}
			else{
				ReadWaveData();
				ReplaceWaveTitle("Wave " + currentWave.WaveNumber);
				StartWave();
			}
		}
	}

}
