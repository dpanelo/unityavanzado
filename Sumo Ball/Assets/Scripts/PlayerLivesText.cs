﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Serializable]
///<summary>
//PlayerLostLife(playerNumber, currentLives)
///</summary>
public class PlayerLostLife : UnityEvent<int, int>{};

public class PlayerLivesText : MonoBehaviour {
	public int playerAssigned;
	public static PlayerLostLife PlayerLostLife;
	private Text livesText;
	private bool assigned;

	void Awake(){
		livesText = GetComponent<Text>();
	}

	// Use this for initialization
	void Start () {
		if(PlayerLostLife == null){
			PlayerLostLife = new PlayerLostLife();
		}

		assigned = false;
		PlayerLostLife.AddListener(eventListener);
	}
	
	private void eventListener(int playerNumber, int currentPlayerLives){
		if(playerNumber == playerAssigned){
			livesText.text = "Player " + playerAssigned + " lives: " + currentPlayerLives;
		}
	}

	public bool IsAssigned(){
		return assigned;
	}

	public void AssignPlayer(bool playerAssigned){
		assigned = playerAssigned;
	}
}
