﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StatusUpdatedEvent : UnityEvent<string>{}

public class StatusTextController : MonoBehaviour {
	private Text statusText;
	public static StatusUpdatedEvent StatusUpdated;

	void Awake(){
		statusText = GetComponent<Text>();
	}

	// Use this for initialization
	void Start () {
		if(StatusUpdated == null){
			StatusUpdated = new StatusUpdatedEvent();
		}

		StatusUpdated.AddListener(StatusUpdatedListener);
	}
	
	private void StatusUpdatedListener(string message){
		statusText.text = message;
	}
}
