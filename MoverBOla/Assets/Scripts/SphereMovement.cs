﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SphereMovement : MonoBehaviour {

	private Rigidbody rigidbody;
	private Transform camera;
	public float acceleration;
	private float horizontal, vertical;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		camera = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		vertical = Input.GetAxisRaw("Vertical");
		horizontal = Input.GetAxisRaw("Horizontal");
	}

	void FixedUpdate(){
		/*if(vertical != 0f){
			Vector3 vectorFromCamera = transform.position - camera.position;
			Vector3 forward = new Vector3(vectorFromCamera.x, 0,vectorFromCamera.z).normalized;

			if(vertical > 0f){
			rigidbody.AddForce(forward * acceleration, ForceMode.Acceleration);
			}
			else{
			rigidbody.AddForce(forward * acceleration * (-1f), ForceMode.Acceleration);
			}
		}*/

		Vector3 vectorFromCamera = transform.position - camera.position;
		Vector3 forward = new Vector3(vectorFromCamera.x, 0,vectorFromCamera.z).normalized;

		rigidbody.AddForce(vertical * forward * acceleration + horizontal * camera.right * acceleration);
	}
}
