﻿using UnityEngine;
using UnityEditor;

public class ClearPlayerPrefs {

	[MenuItem("Custom Tools/Clear Player Preferences")]
	public static void ClearPlayerPreferences(){
		PlayerPrefs.DeleteAll();
	}
}
