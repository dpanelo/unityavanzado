﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ConnectionManager : NetworkBehaviour {

	public InputField PlayerNameInput;
	public InputField PlayerServerNameInput;
	public InputField PlayerServerPortInput;
	public InputField PlayerServerPasswordInput;
	public InputField ServerPortInput;
	public InputField ServerPasswordInput;
	public InputField ChatInput;
	public Text Console;

	private Dictionary<string, string> Players;

	void Start(){
		Players = new Dictionary<string, string>();
	}

	#region Server Methods
	/*public void StartServer(){
		Console.text += "\nStarting Server...";

		Network.incomingPassword = ServerPasswordInput.text ?? "";
		var error = Network.InitializeServer (10, int.Parse(ServerPortInput.text), false);

		if (error != NetworkConnectionError.NoError) {
			Debug.Log ("Failed to initialize Server: " + error.ToString());

			Console.text += "Failed to initialize Server: " + error.ToString();
		}
	}*/

	public override void OnStartServer(){
		Debug.Log ("Server Started");
		Console.text += "\nServer initialized";
	}

	public void OnPlayerConnected(NetworkPlayer player){
		//TODO - Cambiar a stringbuilder
		Debug.Log("Player connected");
		var completeMessage = "\nPlayer Connected, Ip: " + player.ipAddress + ".";
		Console.text += completeMessage;

		//RpcPlayerReceiveChat (completeMessage);
	}

	public void OnPlayerDisconnected(NetworkPlayer player){
		var playerName = Players [player.ipAddress];

		if (!string.IsNullOrEmpty (playerName)) {
			Console.text += "\nPlayer " + playerName + " disconnected.";
		} else {
			Console.text += "\nPlayer ip " + player.ipAddress + " disconnected.";
		}
	}
	#endregion Server Methods

	#region Player Methods
	public void OnConnectedToServer(){
		Console.text += "\nConnected to server: " + PlayerServerNameInput.text;

		//CmdChangePlayerName(connectionToServer.address, PlayerNameInput.text);
	}

	public void OnDisconnectedFromServer(NetworkDisconnection info){
		if (!isLocalPlayer)
            Console.text = "Local server connection disconnected";
        else
            if (info == NetworkDisconnection.LostConnection)
                Console.text = "Lost connection to the server";
            else
                Console.text = "Successfully diconnected from the server";
	}

	/*public void ConnectToServer(){
		Console.text += "\nConnecting to server: " + PlayerServerNameInput.text;
		var error = Network.Connect (PlayerServerNameInput.text, int.Parse (PlayerServerPortInput.text), PlayerServerPasswordInput.text);

		if (error != NetworkConnectionError.NoError) {
			Console.text += "\nConnection failed. " + error.ToString ();
		}
	}*/

	public void OnFailedToConnect(NetworkConnectionError error){
		Console.text += "\n" + error.ToString();
	}

	public void SendChatButtonClicked(){
		//CmdSendChat(connectionToServer.address, ChatInput.text);
		ChatInput.text = "";
	}
	#endregion Player Methods

	//#region Commands
	/*[Command]
	public void CmdSendChat(string playerIp, string message){
		//TODO - Cambiar a stringbuilder
		var completeMessage = "\n" + Players[playerIp] + ": " + message;
		Console.text += completeMessage;
		RpcPlayerReceiveChat (completeMessage);
	}

	[Command]
	public void CmdChangePlayerName(string ip, string name){
		if(!Players.ContainsKey(ip)){
		Players.Add(ip, name);
		}
		else{
			Players[ip] = name;
		}

		//TODO - Cambiar a stringBuilder
		var completeMessage = "\nPlayer from ip" + ip + ", changed their name to: " + name;

		Console.text += completeMessage;

		RpcPlayerReceiveChat (completeMessage);
	}
	#endregion

	#region ClientRPC
	[ClientRpc]
	public void RpcPlayerReceiveChat(string message){
		Console.text += message;
	}
	#endregion*/
}
