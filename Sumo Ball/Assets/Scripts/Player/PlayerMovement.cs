﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : NetworkBehaviour {
	private Rigidbody rigidbody;
	[SyncVar]
	private float verticalInput;
	[SyncVar]
	private float horizontalInput;
	private bool isMoving;
	public float speed;
	
	// Use this for initialization
	void Start () {
		if(!isLocalPlayer){
			this.enabled = false;
		}
		rigidbody = GetComponent<Rigidbody>();
		isMoving = false;
	}
	
	// Update is called once per frame
	void Update () {
		verticalInput = Input.GetAxisRaw("Vertical");
		horizontalInput = Input.GetAxisRaw("Horizontal");

		if(verticalInput != 0 || horizontalInput != 0){
			isMoving = true;
		}
		else{
			isMoving = false;
		}
	}

	[ServerCallback]
	void FixedUpdate(){
		if(isMoving){
			Vector3 verticalMovement = Vector3.forward;
			Vector3 horizontalMovement = Vector3.right;

			rigidbody.AddForce(((verticalMovement * verticalInput) + (horizontalMovement * horizontalInput)) * speed, ForceMode.Acceleration);
			RpcSyncRbInClient(rigidbody.velocity, rigidbody.angularVelocity);
		}
	}

	[ClientRpc]
	void RpcSyncRbInClient(Vector3 velocity, Vector3 angularVelocity){
		rigidbody.velocity = velocity;
		rigidbody.angularVelocity = angularVelocity;
	}

}
