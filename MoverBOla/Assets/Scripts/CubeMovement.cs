﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour {
	private float vertical, halfheight;
	private MeshRenderer renderer;

	void Awake(){
		renderer = GetComponent<MeshRenderer>();
		halfheight = renderer.bounds.extents.y;
	}
	// Update is called once per frame
	void Update () {
		vertical = Input.GetAxisRaw("Vertical");
		PositionCube();
		transform.Translate(Vector3.forward * vertical * Time.deltaTime);
	}

	void PositionCube(){
		RaycastHit hit;
		Physics.Raycast(transform.position, -transform.up, out hit);
		Debug.Log("point: " + hit.point + " normal: " + hit.normal);
		transform.up = hit.normal;
		transform.position = hit.point + hit.normal * halfheight;
		
	}
}
