﻿using UnityEditor;
using UnityEngine;
using System.IO;

public class CreateAssetBundles{
	
	[MenuItem("Custom Tools/Build Asset Bundles")]
	public static void BuildBundles(){
		var directorio = Application.streamingAssetsPath + "/assets";
		if(!Directory.Exists(directorio)){
			Directory.CreateDirectory(directorio);
		}
		
		BuildPipeline.BuildAssetBundles(directorio, 
										BuildAssetBundleOptions.None, 
										BuildTarget.StandaloneWindows64);
	}
}
