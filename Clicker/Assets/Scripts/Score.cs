﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public class ScoreIncreasedEvent : UnityEvent<int>
{
}

public class Score : MonoBehaviour {
	private Text scoreTextGUI;
	private const string scoreString = "Score: ";

	private int totalScore;

	public static ScoreIncreasedEvent ScoreIncreased;

	void Awake(){
		scoreTextGUI = GetComponent<Text>();
	}
	
	// Use this for initialization
	void Start () {
		totalScore = 0;

		SetScoreText();

		if(ScoreIncreased == null){
			ScoreIncreased = new ScoreIncreasedEvent();
		}

		ScoreIncreased.AddListener(ScoreIncreasedListener);
	}
	
	private void ScoreIncreasedListener(int newScore){
		totalScore += newScore;
		SetScoreText();
	}

	private void SetScoreText(){
		scoreTextGUI.text = scoreString + totalScore.ToString();
	}

	public int GetScore(){
		return totalScore;
	}
}
