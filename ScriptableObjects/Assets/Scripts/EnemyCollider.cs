﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EnemyCollider : MonoBehaviour {

	private WaveManager manager;

	public void SetWaveManager(WaveManager someManager){
		manager = someManager;
	}

	void OnTriggerExit2D(Collider2D other){
		Debug.Log("Enemy Destroyed");
		manager.EnemyDestroyed();
		gameObject.SetActive(false);
	}
}
