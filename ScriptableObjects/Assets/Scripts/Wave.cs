﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Wave"
					, menuName = "Custom Data/Wave Data")]
public class Wave : ScriptableObject {
	public string WaveNumber;
	public int RocksNumber;
	public int ShipsNumber;
}
