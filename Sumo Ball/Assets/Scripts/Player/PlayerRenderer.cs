﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(NetworkIdentity))]
public class PlayerRenderer : NetworkBehaviour {
	private MeshRenderer renderer;
	
	// Use this for initialization
	void Start () {
		renderer = GetComponent<MeshRenderer>();

		if(!isLocalPlayer){
			renderer.material.color = Color.blue;
		}
	}
}
