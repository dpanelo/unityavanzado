﻿using System.Collections;
using System.Text;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerPoints : MonoBehaviour {
	public Text scoreText;
	private float puntos;
	private const string literalScore = "Score: ";
	private StringBuilder sb;
	// Use this for initialization
	void Start () {
		puntos = 0;
		sb = new StringBuilder (puntos.ToString());
	}

	public void DarPuntos(float masPuntos){
		puntos += masPuntos;

		ActualizarTexto ();
	}

	void ActualizarTexto(){
		sb.Replace (sb.ToString(), puntos.ToString ());

		scoreText.text = literalScore + sb.ToString();
	}
}